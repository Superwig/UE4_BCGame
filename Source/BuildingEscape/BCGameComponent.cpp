// Copyright (c) 2016 Daniel Marshall

#include "BuildingEscape.h"
#include "BCGameComponent.h"


// Sets default values for this component's properties
UBCGameComponent::UBCGameComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
	Reset();
}


// Called when the game starts
void UBCGameComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UBCGameComponent::Reset()
{
	HiddenWord = TEXT("planet");
	MaxTries = 5;
	CurrentTry = 1;
	bGameIsWon = false;
}

int32 UBCGameComponent::GetMaxTries() const
{
	return MaxTries;
}

int32 UBCGameComponent::GetCurrentTry() const
{
	return CurrentTry;
}

int32 UBCGameComponent::GetHiddenWordLength() const
{
	return HiddenWord.Len();
}

bool UBCGameComponent::IsGameWon() const
{
	return bGameIsWon;
}

EGuessStatus UBCGameComponent::CheckGuess(FString Guess) const
{
	if (!IsIsogram(Guess)) // if the guess isn't an isogram
	{
		return EGuessStatus::Not_Isogram;
	}
	else if (!IsLowercase(Guess)) // if the guess isn't all lowercase
	{
		return EGuessStatus::Not_Lowercase;
	}
	else if (Guess.Len() != GetHiddenWordLength()) // if the guess length is wrong
	{
		return EGuessStatus::Wrong_Length;
	}
	else
	{
		return EGuessStatus::OK;
	}
}

bool UBCGameComponent::IsIsogram(FString Word) const
{
	TSet<char> LetterSeen; // setup our map
	for (auto Letter : Word) 	// for all letters of the word
	{
		Letter = tolower(Letter);
		bool Set;
		LetterSeen.Add(Letter, &Set);
		if (Set)
			return false;
	}
	return true; 
}

bool UBCGameComponent::IsLowercase(FString Word) const
{
	for (auto Letter : Word)
	{
		if (!islower(Letter)) // if not a lowercase letter
		{
			return false;
		}
	}
	return true;
}

FBullCowCount UBCGameComponent::SubmitGuess(FString Guess)
{
	CurrentTry++;
	FBullCowCount BullCowCount;
	int32 WordLength = HiddenWord.Len(); // assuming same length as guess
	// loop through all letters in the hidden word
	for (int32 Iter = 0; Iter < WordLength; ++Iter) 
	{
		int32 Index = Iter;
		if (HiddenWord.FindChar(Guess[Iter], Index))
			if (Iter == Index)
				++BullCowCount.Bulls;
			else
				++BullCowCount.Cows;
	}
	if (BullCowCount.Bulls == WordLength) {
		bGameIsWon = true;
	}
	else
	{
		bGameIsWon = false;
	}
	return BullCowCount;
}