// Fill out your copyright notice in the Description page of Project Settings.

#include "BuildingEscape.h"
#include "BuildingEscapeGameMode.h"

ABuildingEscapeGameMode::ABuildingEscapeGameMode() : Super()
{
	//set default pawn class to Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;
}