// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "BuildingEscapeGameMode.generated.h"

/**
 * 
 */
UCLASS(minimalapi)
class ABuildingEscapeGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	ABuildingEscapeGameMode();	
	
};
