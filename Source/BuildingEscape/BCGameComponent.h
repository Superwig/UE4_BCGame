// Copyright (c) 2016 Daniel Marshall

#pragma once

#include "Components/ActorComponent.h"
#include "BCGameComponent.generated.h"


UENUM()        //"BlueprintType" is essential specifier
enum class EGuessStatus : uint8
{
	Invalid_Status	UMETA(DisplayName = "Invalid"),
	Not_Isogram		UMETA(DisplayName = "Not an isogram"),
	Wrong_Length	UMETA(DisplayName = "Wrong length"),
	Not_Lowercase	UMETA(DisplayName = "Not lowercase"),
	OK				UMETA(DisplayName = "Okay")
};

USTRUCT(BlueprintType)
struct FBullCowCount
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadOnly)
	int32 Bulls = 0;
	UPROPERTY(BlueprintReadOnly)
	int32 Cows = 0;
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UBCGameComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UBCGameComponent();

	// Called when the game starts
	virtual void BeginPlay() override;

	// Called every frame
	//virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	UFUNCTION(BlueprintCallable, Category = "Game")
	int32 GetMaxTries() const;
	UFUNCTION(BlueprintCallable, Category = "Game")
	int32 GetCurrentTry() const;
	UFUNCTION(BlueprintCallable, Category = "HiddenWord")
	int32 GetHiddenWordLength() const;
	UFUNCTION(BlueprintCallable, Category = "Game")
	bool IsGameWon() const;
	UFUNCTION(BlueprintCallable, Category = "Guess")
	EGuessStatus CheckGuess(FString Guess) const;
	UFUNCTION(BlueprintCallable, Category = "Guess")
	FBullCowCount SubmitGuess(FString Guess);
	void Reset();

private:
	int32 CurrentTry;
	UPROPERTY(EditDefaultsOnly)
	int32 MaxTries;
	UPROPERTY(EditDefaultsOnly)
	FString HiddenWord;
	
	bool bGameIsWon;

	bool IsIsogram(FString) const;
	bool IsLowercase(FString) const;	
};
